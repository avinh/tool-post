const jwt = require('jsonwebtoken');
const { JWT } = require('../config/app');
// const accountService = require('../api/account/service');
module.exports =  (route) => {
  route.use((req, res, next) => {
    const token = req.headers['Authorization'] || req.headers['authorization'];
    // decode token
    if (token) {
      let arrayToken = token.split(' ');
      if(arrayToken[0] !== 'Bearer'){
        return res.status(403).send({
          'error'  : true,
          'message': 'Token is wrong format'
        });
      }
      // verifies secret and checks exp
      jwt.verify(arrayToken[1], JWT.SECRET,  (err, decoded)=> {
        if (err) {
          return res.status(401).json({ 'error': true, data: {alive: false}, 'message': 'Unauthorized access.' });
        }
        req.user = decoded;
        next();
      });
    } else {
      // if there is no token
      // return an error
      return res.status(403).send({
        'error'  : true,
        'message': 'Missing accessToken in header'
      });
    }
  });
  return route;
};