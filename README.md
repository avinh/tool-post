# REGISTER
  - Front end gọi api `register` để tạo account
  - Server sẽ gửi mail tới email sử dụng để đăng ký
  - User click vào link trong gmail để kích hoạt account
# Login
  - Sau khi đăng ký và kích hoạt có thể sử dụng account để vào hệ thống
# ResetPasswor
  - FE gọi api `forgot` password
  - Server sẽ trả về đường link để dẫn qua trang của FE, để FE render form để nhập thông tin `password` mới (có kèm thêm `token` dùng để gọi reset password) 
  - Sau khi user nhập thông tin `password` mới FE gửi `password` mới kèm token lên server vs api `{{facebookurl}}/api/v1/reset`
  - Hoàn tất resetpassword
# Tạo account facebook
  - có các api tạo account user facebook trong thử mực User Facebook những account này dùng để lấy thông tin bài post ( Chi tiết trong api doc theem xoa sua list)
# Get List Fanpage
  - List ra tất cả các fanpage & group của tất cả các user facebook của account đó add vào 
# Cách Add fanpage vào workplace
  - Phải tạo user facebook và workplace trước
  - Get list fanpage 
  - call api add fanpage vào workplace
  - các fanpage và group sẽ được lưu vào workplace
# Setting sync post hiện tại đang support theo 1 số khung h (chỉ admin ms được cấu hinh)