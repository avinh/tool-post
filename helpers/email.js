
const nodemailer = require('nodemailer');
const { EMAIL_CONFIG } = require('../config/app');
const log = require('../lib/Log.class');
module.exports = {
  sendMailTemplate: function (to, data, type){
    return new Promise((resolve, reject) => {
      if (!to || !type) {
        log.error('Missing require fields');
      }
      let subject = '';
      let emailTemplate = {};
      switch (type) {
      case 'REGISTER': subject = 'Invitaion'; break;
      case 'REGISTER_SUCCESS': subject = 'REGISTER_SUCCESS'; break;
      case 'RESET_PASSWORD': subject = 'Reset Password'; break;
      default: log.error('Can\'t find emplate email'); return;
      }
      emailTemplate.subject = subject;
      emailTemplate.body = global.ectRenderer.render(`emailTemplate/${type}`, { data });
      // seding email
      return this.sendMail(EMAIL_CONFIG.USER,
        EMAIL_CONFIG.USER, to, null,
        null, emailTemplate.subject, emailTemplate.body).then(() => {
        return resolve({ success: true, message: `Send reset password to ${to}: Success` });
      }).catch((e) => {
        log.err(e.message);
        return reject({ success: false, message: `Send reset password to ${to}: Fail` });
      });
    });
  },
  sendMail: (fromname, from, to, cc, bcc, subject, body, attachmentArray) => {
    /*
         * Ex: sending.sendMail('ADMIN MY_PROJECT', 'ADMIN', 'khoa.dinh@dinovative.com', null, null, 'subject', 'body', null)
         * attachmentArray
         * https://github.com/nodemailer/nodemailer#attachments
         * [{
         *    path: '/path/to/<file_name>.doc'
         * }]
         *
        */
    return new Promise((resolve, reject) => {
      let smtpConfig = {
        host  : EMAIL_CONFIG.HOST,
        port  : EMAIL_CONFIG.PORT,
        secure: EMAIL_CONFIG.SMTP_SECURE,
        auth  : {
          user: EMAIL_CONFIG.USER,
          pass: EMAIL_CONFIG.PASS
        }
      };

      let transporter = nodemailer.createTransport(smtpConfig);
      let mailOptions = {
        from   : from,
        to     : to,
        subject: subject,
        html   : body,
      };

      if (attachmentArray && attachmentArray.length > 0) mailOptions.attachments = attachmentArray;
      // if (fromname && fromname.length > 0) {
      //   mailOptions.from = '"' + fromname + '" <' + from + '>'
      // }
      if (cc) mailOptions.cc = cc;
      if (bcc) mailOptions.bcc = bcc;

      return transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          log.error('sendMail Error 1: ', error);
          return reject(error);
        }
        log.success('Send Mail success. info: ', info);
        return resolve(info);
      });
    }).catch((error) => {
      log.error('sendMail Catch Error: ', error);
      return false;
    });
  }
};
