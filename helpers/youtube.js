const axios = require('axios');
const ultility = require('../helpers/utilities');
const { YOUTUBE } = require('../config/app');
const APIError = require('../lib/APIError');
const { ERROR_TYPE } = require('../constant/index');
const Log = require('../lib/Log.class');
let buildYoutubeUrl = (route, query) => {
  let url = `${YOUTUBE.URL}/${route}?${ultility.buildUrl(query, 'snake')}&key=${YOUTUBE.KEY}`;
  Log.info(`Request ${url}`);
  return url;
};

module.exports = {
  searchList: async (params) => {
    try {
      const response = await axios({
        method: 'get',
        url   : `${buildYoutubeUrl('search', params)}`,
      });
      return response;
    } catch(e) {
      throw new APIError(e.message, 500, ERROR_TYPE.CALLING_YOUTUBE_FAIL);
    }
  },
  getDetail: async (videoId) => {
    try {
      const params = {
        part: 'statistics, snippet',
        id  : videoId
      };
      const response = await axios({
        method: 'get',
        url   : `${buildYoutubeUrl('videos', params)}`,
      });
      return response;
    } catch (e) {
      throw new APIError(e.message, 500, ERROR_TYPE.CALLING_YOUTUBE_FAIL);

    }
  },
  getChanel: async (query) => {
    try {
      const response = await axios({
        method: 'get',
        url   : `${buildYoutubeUrl('channels', query)}`,
      });
      return response;
    } catch (e) {
      throw new APIError(e.message, 500, ERROR_TYPE.CALLING_YOUTUBE_FAIL);

    }
  }
};
