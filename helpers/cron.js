const CONSTANT = require('../constant/index');
const cronPostService = require('../api/post/service');
const cronChannelService = require('../api/channels/service');
const settingsService = require('../api/settings/service');
const CronJobManager = require('cron-job-manager');
const manager = new CronJobManager();
const Logs = require('../lib/Log.class');


module.exports = {
  startCronJob: async () => {
    Logs.success('CronJob is starting');
    if (CONSTANT.ACTIVE_CRON_POST) {
      manager.add('schedulePost', CONSTANT.TIME_RUN_CRON, async function () {
        await cronPostService.startCronPost();
      });
      manager.start('schedulePost');
    }
    const setting = await settingsService.find();
    if (setting && setting.length) {
      if (setting[0] && setting[0].enable === true) {
        manager.add('syncPost', CONSTANT.CRON_SETTING[setting[0].timeToSync], async function () {
          await cronPostService.syncPostMultiFanpage(setting[0].limitSync);
          await cronChannelService.syncVideoMultiChannel(setting[0].limitSync);
        });
        manager.start('syncPost');
      }
    }
  },
  manager
};
