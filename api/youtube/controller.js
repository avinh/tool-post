const youtubeService = require('./service');
const channelService = require('../channels/service');
const workplaceService = require('../workplace/service');
const utilities = require('../../helpers/utilities');
const { DEFAUL_LIMIT } = require('../../config/app');
const { MESSAGE } = require('../../constant');
const _ = require('lodash');

module.exports = {
  getListDb: async(req, res)=>{
    try{
      let query = req.query;
      let viewCountMin = utilities.getValidString(query.viewCountMin);
      let viewCountMax = utilities.getValidString(query.viewCountMax);
      let likeCountMin = utilities.getValidString(query.likeCountMin);
      let likeCountMax = utilities.getValidString(query.likeCountMax);
      let dislikeCountMin = utilities.getValidString(query.dislikeCountMin);
      let dislikeCountMax = utilities.getValidString(query.dislikeCountMax);
      let commentCountMin = utilities.getValidString(query.commentCountMin);
      let commentCountMax = utilities.getValidString(query.commentCountMax);
      let limit = query.limit || DEFAUL_LIMIT;
      let page = query.page || 1;
      let skip = utilities.getMongoSkip(page, limit);
      let params = {
        title       : utilities.getValidString(query.title),
        isBookmark  : utilities.booleanValidate(query.isBookmark),
        channel     : utilities.getValidString(query.channel),
        channelTitle: utilities.getValidString(query.channelTitle),
        viewCount   : setMinMaxQuery(viewCountMin, viewCountMax),
        likeCount   : setMinMaxQuery(likeCountMin, likeCountMax),
        dislikeCount: setMinMaxQuery(dislikeCountMin, dislikeCountMax),
        commentCount: setMinMaxQuery(commentCountMin, commentCountMax),
      };
      params = utilities.buildFullTextSearchObj(params, ['title']);
      params = utilities.removeEmptyFields(params);
      if(!params.channel){
        let workplaces = await workplaceService.find({createBy: req.user._id, type: 'youtube'}) || [];
        let workplacesIds = workplaces.map(e => e._id);
        if(workplacesIds || workplacesIds.length === 0){
          return res.success({youtubes: []});
        }
        let channels = await channelService.find({workplaces: {$in: workplacesIds}}) || [];
        let channelIds = channels.map(e => e._id);
        if(channelIds || channelIds.length === 0){
          return res.success({youtubes: []});
        }
        params.channel = {$in: channelIds};
      }
      let youtubes = await youtubeService.find(params, {limit, skip });
      return res.success(youtubes);
    }catch(e){
      return res.error(e, 500);
    }
  },
  getList: async (req, res)=>{
    try{
      let query = req.query;
      let params = {
        q         : utilities.getValidString(query.search),
        maxResults: DEFAUL_LIMIT,
        pageToken : utilities.getValidString(query.nextPageToken) || utilities.getValidString(query.prevPageToken),
        order     : utilities.getValidString(query.order),
        type      : 'video',
        part      : 'snippet'
      };
      let option = {
        viewCount   : utilities.getValidString(query.viewCount),
        likeCount   : utilities.getValidString(query.likeCount),
        dislikeCount: utilities.getValidString(query.dislikeCount)
      };
      params = utilities.removeEmptyFields(params);
      option = utilities.removeEmptyFields(option);
      if (!params && !params.search) {
        return res.error('Please enter key in input search');
      }
      let listResult = await youtubeService.getListVideo(params);
      if (!listResult) {
        return res.error('No result');
      }
      let promise = [];
      const nextPageToken = _.get(listResult, 'data.nextPageToken');
      const prevPageToken = _.get(listResult, 'data.prevPageToken');
      const total = _.get(listResult, 'data.pageInfo.totalResults');
      let listVideo = _.get(listResult, 'data.items');
      if (listVideo) {
        const listVieoId = listVideo.map(i => i.id.videoId);
        listVieoId.map(id => promise.push(youtubeService.getDetailVideo(id)));
        const result = await Promise.all(promise);
        listVideo = _.flatMap(result, i => i.data.items);
        if (option && option.viewCount) {
          listVideo = listVideo.filter(i => parseInt(i.statistics.viewCount) > parseInt(option.viewCount));
        }
        if (option && option.likeCount) {
          listVideo = listVideo.filter(i => parseInt(i.statistics.likeCount) > parseInt(option.likeCount));
        }
        if (option && option.dislikeCount) {
          listVideo = listVideo.filter(i => parseInt(i.statistics.dislikeCount) > parseInt(option.dislikeCount));
        }
      }
      listVideo = _.map(listVideo, i => utilities.formatValueYoutube(i));
      return res.success({ total, listVideo, nextPageToken, prevPageToken }, MESSAGE.GET_SUCCESS, 200);
    } catch(e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
  getListBookMart: async (req, res) => {
    try {
      let query = req.query;
      let limit = utilities.getValidNumber(query.limit) || DEFAUL_LIMIT;
      let page = utilities.getValidNumber(query.page) || 1;
      let skip = utilities.getMongoSkip(page, limit);
      const [data, total] = await Promise.all([
        youtubeService.find({ isBookmark: true }, { skip, limit }),
        youtubeService.count({ isBookmark: true })
      ]);
      let totalPage = utilities.getTotalPage(total, limit);
      return res.success({ page, totalPage, limit, total, data }, MESSAGE.GET_SUCCESS, 200);
    } catch (e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
  insert: async (req, res) => {
    try {
      let body = req.body;
      let data = {
        id           : utilities.getValidString(body.id),
        isBookmark   : true,
        title        : utilities.getValidString(body.title),
        description  : utilities.getValidString(body.description),
        viewCount    : utilities.getValidString(body.viewCount),
        likeCount    : utilities.getValidString(body.likeCount),
        dislikeCount : utilities.getValidString(body.dislikeCount),
        favoriteCount: utilities.getValidString(body.favoriteCount),
        commentCount : utilities.getValidString(body.commentCount),
        publishedAt  : utilities.getValidString(body.publishedAt),
        channelId    : utilities.getValidString(body.channelId),
        channelTitle : utilities.getValidString(body.channelTitle),
      };
      data = utilities.removeEmptyFields(data);
      if (!body && !body.id) {
        return res.error('missing require filed id.');
      }
      if (!body && !body.publishedAt){
        return res.error('missing require filed publishedAt.');
      }
      const dataFound = await youtubeService.findOne({ id: body.id });
      if (dataFound ) {
        return res.error('video youtube exists already.');
      }
      const channel = await channelService.findOne({ channelId: data.channelId});
      if (!channel ) {
        return res.error('channelId youtube isn\'t exists.');
      }
      data.attachments = body.attachments;
      data.channel = channel._id;
      const result = await youtubeService.insertYoutube(data);
      return res.success(result);
    } catch (e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
  updatePostYoutube: async (req, res) => {
    try {
      const postYoutubeId = req.params.id;
      if (!postYoutubeId) {
        return res.error('missing require field id.');
      }
      let body = req.body;
      let data = {
        title        : utilities.getValidString(body.title),
        description  : utilities.getValidString(body.description),
        viewCount    : body.viewCount ? Number(body.viewCount) : 0,
        likeCount    : body.likeCount ? Number(body.likeCount) : 0,
        dislikeCount : body.dislikeCount ? Number(body.dislikeCount) : 0,
        favoriteCount: body.favoriteCount ? Number(body.favoriteCount) : 0,
        commentCount : body.commentCount ? Number(body.commentCount) : 0,
        publishedAt  : utilities.getValidDate(body.publishedAt),
        channelId    : utilities.getValidString(body.channelId),
        channelTitle : utilities.getValidString(body.channelTitle),
        isBookmark   : body.isBookmark
      };
      data = utilities.removeEmptyFields(data);
      data.attachments = body.attachments;
      let dataPost = await youtubeService.findOne({ id: postYoutubeId });
      if (!dataPost) {
        return res.error('Post of Youtube doesn\'t exists.');
      }
      for (let key in data) {
        dataPost[key] = data[key];
      }
      dataPost = await dataPost.save();
      return res.success(dataPost);
    } catch (e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
};

let setMinMaxQuery = (min, max) => {
  if(!min && !max){
    return;
  }
  let tmp = {};
  if(min !== undefined && min !== null){
    tmp['$gte'] = min;
  }
  if(max !== undefined && max !== null){
    tmp['$lte'] = max;
  }
  return tmp;
};
