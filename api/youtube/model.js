const mongoose = require('mongoose');
const { STATUS } = require('../../constant/');
let YoutubeSchema = new mongoose.Schema({
  id            : {type: String, required: true, unique: true},
  status        : {type: String, enum: STATUS.ENUM, default: STATUS.ACTIVE},
  isBookmark    : {type: Boolean, default: false},
  title         : {type: String},
  description   : {type: String},
  viewCount     : {type: Number, default: 0},
  likeCount     : {type: Number, default: 0},
  dislikeCount  : {type: Number, default: 0},
  favoriteCount : {type: Number, default: 0},
  commentCount  : {type: Number, default: 0},
  publishedAt   : {type: Date, required: true},
  channel       : {type: mongoose.Schema.Types.ObjectId, ref: 'Channel' },
  channelId     : {type: String, required: true },
  channelTitle  : {type: String},
  isPostFacebook: {type: Boolean, default: false},
  attachments   : [{ type: String }],
}, {
  timestamps: true,
  toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
    }
  },
});

module.exports = mongoose.model('Youtube', YoutubeSchema);
