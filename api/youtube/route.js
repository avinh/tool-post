
const youtubeController = require('./controller');

module.exports = (route) =>{
  route.get('/youtube/list', youtubeController.getListDb);
  route.get('/youtube', youtubeController.getList);
  route.get('/youtube/bookmark', youtubeController.getListBookMart);
  route.post('/youtube', youtubeController.insert);
  route.put('/youtube/:id', youtubeController.updatePostYoutube);
  return route;
};
