const youtubeModel = require('./model');
const youtubeHelper = require('../../helpers/youtube');

module.exports = {
  find: async (query, option) => {
    if(option){
      return youtubeModel.find(query).skip(option.skip).limit(option.limit).populate('channel');
    }
    return await youtubeModel.find(query).populate('channel');
  },
  count: async (query) => {
    return youtubeModel.countDocuments(query);
  },
  insertYoutube: async (data) => {
    return youtubeModel.create(data);
  },
  findOne: async (query) => {
    return youtubeModel.findOne(query);
  },
  getListVideo: async (query) =>{
    return await youtubeHelper.searchList(query);
  },
  getDetailVideo: async (id) => {
    return await youtubeHelper.getDetail(id);
  },
  remove: async (query) => {
    return await youtubeModel.remove(query);
  }
};
