const userService = require('./service');
const fanpageService = require('../fanpage/service');
const utilities = require('../../helpers/utilities');
const facebookHelper = require('../../helpers/facebook');
const { DEFAUL_LIMIT } = require('../../config/app');
const {  MESSAGE, STATUS }  = require('../../constant');
module.exports = {
  createUser: async(req, res) => {
    try {
      let body = req.body;
      let data = {
        accessToken: utilities.getValidString(body.accessToken),
      };
      if(!data.accessToken){
        return res.error('Missing require field', 400);
      }
      let resultFb = await facebookHelper.getProfile({ access_token: data.accessToken, fields: 'id,name,picture,email'});
      if(!resultFb){
        return res.error('Accesstoken is invalid', 400);
      }
      let user = await userService.findOne({ email: resultFb.email, createdBy: req.user._id });
      if(user && user.status !== STATUS.DELETE){
        return res.error('Email is already exist', 400);
      }
      data.status = STATUS.ACTIVE;
      data.name = resultFb.name;
      data.email = resultFb.email;
      data.facebookId = resultFb.id;
      data.picture = resultFb.picture;
      data.createdBy = req.user._id;
      data.isTokenFBExpire = false;
      if (!utilities.isValidEmail(data.email)) {
        return res.error('Email is not valid', 400);
      }
      //let admin = await userService.getAccountAdmin();
      //set token active account
      //data.activeAccountToken = utils.generateRandomString(20, (new Date).getTime());
      if(user) {
        utilities.replaceField(user, data);
        await user.save();
      } else{
        user = await userService.insertUser(data);
      }

      // await emailHelper.sendMailTemplate(admin.email, data, 'REGISTER');
      return res.success(user, MESSAGE.GET_SUCCESS, 200);
    } catch (err) {
      return res.error(err.message, 500);
    }

  },
  updateUser: async (req, res) => {
    try {
      let body = req.body;
      let id = req.params.id;
      let params = {
        accessToken: body.accessToken,
      };
      if(!params.accessToken){
        return res.error('Missing require field(s)', 400);
      }
      let user = await userService.findById(id);
      if(!user){
        return res.error(`User is not found with id =${id}`, 400);
      }
      let responseFB = await facebookHelper.getProfile({access_token: params.accessToken, fields: 'id,name,picture,email'});
      if(!responseFB){
        return res.error('token facebook is not valid', 400);
      }
      user.isTokenFBExpire = false;
      user.accessToken = params.accessToken;
      await user.save();
      return res.success(user, MESSAGE.GET_SUCCESS, 200);
    } catch (e){
      return res.error(e.message, 500);
    }
  },
  deleteUser: async (req, res)=>{
    try {
      let id = req.params.id;
      if(!id){
        return res.error('Missing require id', 400);
      }
      let user = await userService.findById(id);
      if(!user || user.status === STATUS.DELETE) {
        return res.error(`User is not found with id =${id}`, 400);
      }
      await fanpageService.remove({user: user._id});
      await user.remove();
      return res.success({ user }, MESSAGE.DELETE_SUCCESS, 200);
    } catch(e) {
      return res.error(MESSAGE.DELETE_FAIL, 500);
    }

  },
  getList: async (req, res)=>{
    try{
      let query = req.query;
      let limit = utilities.getValidNumber(query.limit) || DEFAUL_LIMIT;
      let page = utilities.getValidNumber(query.page) || 1;
      let skip = utilities.getMongoSkip(page, limit);
      let params = {
        name     : utilities.getValidString(query.name),
        email    : utilities.getValidString(query.email),
        status   : {$ne: STATUS.DELETE},
        createdBy: req.user._id
      };
      let isTokenFBExpire = utilities.booleanValidate(query.isTokenFBExpire);

      // params.createdBy = req.user._id;
      params = utilities.removeEmptyFields(params);
      params = utilities.buildFullTextSearchObj(params, ['name', 'email']);
      let [listUser, total] = await Promise.all([
        userService.find(params, { skip, limit }),
        userService.count(params)
      ]);
      let checkALiveArr = await Promise.all( listUser.map(user => {
        return facebookHelper.checkAliveToken(user.accessToken);
      }));
      for(let index in listUser){
        if(listUser[index]){
          listUser[index].isTokenFBExpire = !checkALiveArr[index];
        }
      }
      if(isTokenFBExpire !== undefined){
        listUser = listUser.filter(e => e.isTokenFBExpire === isTokenFBExpire);
      }
      let totalPage = utilities.getTotalPage(total, limit);
      return res.success({ page, totalPage, limit, total, listUser }, MESSAGE.GET_SUCCESS, 200);
    } catch(e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  }
};