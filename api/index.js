const express = require('express');
const Log = require('../lib/Log.class');
const authMiddleware = require('../middleware/auth');
const user = require('./user/route');
const account = require('./account/route');
const workplace = require('./workplace/route');
const fanpage = require('./fanpage/route');
const post = require('./post/route');
const settings = require('./settings/route');
const images = require('./images/route');

const youtube = require('./youtube/route');
const channel = require('./channels/route');

const authentication = require('./authenticaton/route');

const publicRoute = { authentication };
const privateRoute = { account, workplace, fanpage, user, post, settings, youtube, channel, images };
module.exports = app => {
  try {
    //
    let route = express.Router();
    //init all route
    Log.success('\n');
    for (let key in publicRoute) {
      app.use('/api/v1', publicRoute[key](route));
      Log.success(`PUBLIC - ${key.toUpperCase()} was inited`);
    }
    for (let key in privateRoute) {
      app.use('/api/v1', authMiddleware(route), privateRoute[key](route));
      Log.success(`PRIVATE - ${key.toUpperCase()} was inited`);
    }
    //message
    Log.success('\nRoute was init');
  } catch (e) {
    Log.error(e);
  }
};
