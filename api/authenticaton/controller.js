
const moment = require('moment');
const jwt = require('jsonwebtoken');
const accountService = require('../account/service');
const emailHelper = require('../../helpers/email');
const utilities = require('../../helpers/utilities');
const { ACCOUNT_ROLE, MESSAGE, STATUS }  = require('../../constant');
const { JWT, DOMAIN_FE} =  require('../../config/app');
module.exports = {
  login: async (req, res) => {
    try{
      let email = req.body.email;
      let password = req.body.password;
      let account = await accountService.findOne({ email });
      if(!account){
        return res.error('Account is not found', 400);
      }
      if(account.status !== STATUS.ACTIVE){
        return res.error('Account is not active', 400);
      }
      if (account.comparePassword(password)) {
      // generate access token
        const accessToken = jwt.sign({
          _id        : account._id,
          email      : account.email,
          username   : account.username,
          accessToken: account.accessToken,
          role       : account.role
        }, JWT.SECRET, { expiresIn: JWT.EXPIRE_ACCESS_TOKEN });
        // generate refresh token
        const refreshToken = jwt.sign({
          _id: account._id,
        }, JWT.SECRET, { expiresIn: JWT.EXPIRE_REFRESH_TOKEN });
        return res.success({ accessToken: `Bearer ${accessToken}`, refreshToken, account });
      } else {
        return res.error('Password is not corret', 401);
      }
    } catch(error){
      return res.error(error.message, 500);
    }
  },
  activeAccount: async (req, res) => {
    let activeAccountToken = req.params.token;
    let email = req.params.email;
    let data = await accountService.findOneAndUpdate({ activeAccountToken }, { status: STATUS.ACTIVE, activeAccountToken: undefined });
    if (data) {
      emailHelper.sendMailTemplate(email, {}, 'REGISTER_SUCCESS');
      return res.success();
    } else {
      return res.error('Your token is not found', 400);
    }
  },
  refeshToken: async (req, res) => {
    try{
      let refreshToken = req.body.refreshToken;
      let data =  await jwt.verify(refreshToken, JWT.SECRET);
      let account = await accountService.findById(data._id);
      if (account) {
      // generate access token
        const accessToken = jwt.sign({
          _id  : account._id,
          email: account.email,
          name : account.name,
          role : account.role
        }, JWT.SECRET, { expiresIn: JWT.EXPIRE_ACCESS_TOKEN });
        // generate refresh token
        return res.success({ accessToken: `Bearer ${accessToken}` });
      } else {
        return res.error('Refresh token is expired plesase login again', 401);
      }
    } catch(error) {
      if(error.name === 'JsonWebTokenError'){
        return res.error('Refresh token is invalid', 400);
      }
      return res.error(error.message, 500);
    }
  },
  register: async (req, res) => {
    try {
      let body = req.body;
      let data = {
        email   : utilities.getValidString(body.email),
        password: utilities.getValidString(body.password),
        name    : utilities.getValidString(body.name),
        picture : utilities.getValidString(body.password),
        role    : ACCOUNT_ROLE.CUSTOMER
      };
      if(!data.email || !data.password){
        return res.error('Missing require field', 400);
      }
      let account = await accountService.findOne({ email: data.email });
      if(account){
        return res.error('Email is already exist', 400);
      }
      if (!utilities.isValidEmail(data.email)) {
        return res.error('Email is not valid', 400);
      }
      data.activeAccountToken = utilities.generateRandomString(20, (new Date).getTime());
      let newAccount = await accountService.insertAccount(data);
      data.url = `${DOMAIN_FE}/active-account-token/${data.email}/${data.activeAccountToken}`;

      await emailHelper.sendMailTemplate(data.email, data, 'REGISTER');
      return res.success(newAccount, MESSAGE.GET_SUCCESS, 200);
    } catch (error) {
      res.error(error.message, 500);
    }
  },
  resendEmailInvatation: async (req, res)=>{
    try{
      let email = req.params.email;
      if(!utilities.isValidEmail(email)){
        return res.error('Email is not valid', 400);
      }
      let user = await accountService.findOne({ email, activeAccountToken: { $exists: true } });
      if(!user){
        return res.error('User is not found with email or you already active', 400);
      }
      let data = user.toObject();
      data.url = `${DOMAIN_FE}/active-account-token/${data.email}/${data.activeAccountToken}`;

      await emailHelper.sendMailTemplate(data.email, data, 'REGISTER');
      return res.success('Resend email success', 200);
    } catch(e){
      return res.error(e.message, 500);
    }
  },
  forgot: async (req, res) => {
    try {
      let email = req.body.email;
      let account = await accountService.getAccountByEmail(email);
      if (!account) {
        return res.error('Your Email is not exist in system', 400);
      }
      let token = utilities.generateRandomString(20, (new Date).getTime());
      let expireTime = moment().add(1, 'h');
      account.resetPasswordExpire = expireTime;
      account.resetPasswordToken = token;
      await account.save();
      await emailHelper.sendMailTemplate(account.email, {
        name : account.name,
        email: account.email,
        url  : `${DOMAIN_FE}/reset-password/${token}`
      }, 'RESET_PASSWORD');
      return res.success();
    } catch (error) {
      res.error(error.message, 500);
    }
  },
  resetPassword: async (req, res) => {
    try {
      let token = utilities.getValidString(req.body.token);
      let password = utilities.getValidString(req.body.password);
      let message;
      if (!token || !password) {
        message = 'Token or password is empty';
      }

      if (password && password.length < 6) {
        message = 'Password is at least 6 characters';
      }

      let account = await accountService.findOne({ resetPasswordToken: token });
      if (!account) {
        message = 'Token is invalid';
      }
      // Expire reset token
      if (account && account.resetPasswordExpire < Date.now()) {
        message = 'Token is expire';
        // remove token and expire if token is expire
        account.resetPasswordToken = undefined;
        account.resetPasswordExpire = undefined;
        account.save();
      }
      if (message) {
        return res.error(message, 500);
      }
      account.password = password;
      // remove token and expire if wehn change success
      account.resetPasswordToken = undefined;
      account.resetPasswordExpire = undefined;
      account.save();
      return res.success();
    } catch (error) {
      res.error(error.message, 500);
    }
  }
};