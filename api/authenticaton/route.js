
const { register, forgot, resetPassword,  login, refeshToken, activeAccount, resendEmailInvatation} = require('./controller');

module.exports = (route) =>{
  route.post('/account/resend-email/:email', resendEmailInvatation);
  route.post('/register', register);
  route.post('/forgot', forgot);
  route.post('/refresh-token', refeshToken);
  route.post('/reset', resetPassword);
  route.post('/login', login);
  route.get('/active-account-token/:email/:token', activeAccount);

  return route;
};