
const accountController = require('./controller');

module.exports = (route) =>{
  route.post('/account/change-password', accountController.changePassword);
  route.get('/account', accountController.getList);
  route.post('/account', accountController.createAccount);
  route.put('/account/:id', accountController.updateAccount);
  route.delete('/account/:id', accountController.deleteAccount);
  route.get('/account/check-token', accountController.checkToken);

  return route;
};