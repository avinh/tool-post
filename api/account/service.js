const accountModel = require('./model');
const bcrypt = require('bcrypt-nodejs');

module.exports = {
  find: (query, option) =>{
    return accountModel.find(query).skip(option.skip).limit(option.limit);
  },
  count: (query) =>{
    return accountModel.countDocuments(query);
  },
  insertAccount: async (account) => {
    account.passsword =  bcrypt.hashSync(account.password, bcrypt.genSaltSync(8), null);
    return accountModel.create(account);
  },
  findById: (id)=>{
    return accountModel.findById(id);
  },
  getAccountByEmail: (email)=>{
    return accountModel.findOne({email: email});
  },
  findOne: (param)=>{
    return accountModel.findOne(param);
  },
  getAccountAdmin: ()=>{
    return accountModel.findOne({role: 'Admin'});
  },
  findOneAndUpdate: (query, params)=>{
    return accountModel.findOneAndUpdate(query, params);
  }
};
