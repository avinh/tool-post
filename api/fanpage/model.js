const mongoose = require('mongoose');
const { STATUS, TYPE_FANPAGE } = require('../../constant');
const Schema = mongoose.Schema;
// Time Rule Example
// 09:00 <=> 900 , 20:30 <=> 2030
const FanpageSchema = new Schema({
  name           : { type: String, required: true },
  type           : { type: String, enum: TYPE_FANPAGE.ENUM },
  status         : { type: String, enum: STATUS.ENUM, default: STATUS.ACTIVE },
  accessToken    : { type: String },
  isTokenFBExpire: { type: Boolean, default: true },
  isAdmin        : { type: Boolean, default: false },
  picture        : { type: String },
  fanpageId      : { type: String, required: true },
  user           : { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  workplace      : { type: mongoose.Schema.Types.ObjectId, ref: 'Workplace', required: true },
}, {
  timestamps: true, toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
      delete ret.password;
    }
  },
});

module.exports = mongoose.model('Fanpage', FanpageSchema);
