let facebookHelper = require('../../helpers/facebook');
let Logs = require('../../lib/Log.class');
let _ = require('lodash');
let utility = require('../../helpers/utilities');
let userService = require('../user/service');
let fanpageService = require('../fanpage/service');
// let constants = require('../../constant');
module.exports = {
  getListFanpageByKeyWord: async(req, res)=>{
    try{
      let q = req.query.keyword;
      if(!q){
        return res.error('keywork is require', 400);
      }
      let user = await userService.getUserActive(req.user._id);
      if(!user || !user){
        return res.error('All token of user is expire plase update accessToken for all user', 400);
      }
      let fanpages = await facebookHelper.getListFanpageByKeyWord(q, user.accessToken);
      fanpages = _.get(fanpages, 'data', []);
      fanpages = await Promise.all(fanpages.map(async fp =>{
        try{
          return await facebookHelper.getFanpageInfo(fp.id, {access_token: user.accessToken});
        }catch(e){
          Logs.error(e);
        }
        return;
      }));
      if(fanpages){
        fanpages = fanpages.filter(e => e);
      }
      return res.success(fanpages);
    }catch(e){
      return res.error(e);
    }
  },
  update: async(req, res) => {
    try{
      let id = req.params.fanpageId;
      let userId = req.params.userId;
      let user = await userService.findById(userId);
      if(!user){
        return res.error('user is not found', 400);
      }
      let fanpage = await fanpageService.findById(id);
      if(!fanpage){
        return res.error('fanpage is not found', 400);
      }
      let fanpageFB;
      try {
        fanpageFB = await facebookHelper.getFanpageInfo(fanpage.fanpageId, {access_token: user.accessToken});
      } catch(e){
        return res.error('AcessToken user is expire');
      }
      if(fanpageFB.fanpageId === fanpage.fanpageId){
        fanpage.accessToken = fanpageFB.accessToken || user.accessToken;
        //update for all fanpage have same fanpageId in orther account
        if(fanpageFB.accessToken){
          await fanpageService.updateMany({fanpageId: fanpage.fanpageId}, {accessToken: fanpageFB.accessToke});
        }
        fanpage.user = userId;
        let updateFanpage = await fanpage.save();
        if(!updateFanpage){
          throw new Error('Save fanpage is not success');
        }
        let dataRes = {
          ...updateFanpage.toObject(),
          user: user.toObject(),
        };
        return res.success(dataRes);
      }
      return res.error('AcessToken user is expire');
    } catch(e) {
      return res.error(e, 500);
    }
  },
  getAvailable: async (req, res) => {
    try {
      let params = {
        createdBy: req.user._id
      };
      let users = await userService.find(params);
      if(req.query.name){
        params.name = utility.getValidString(req.query.name);
      }
      if(!users || users.length === 0){
        return res.error('User is not found', 400);
      }
      let arrayFanpages = await Promise.all(
        users.map(async(user) => {
          let responeFb = [];
          try{
            responeFb =  await facebookHelper.getListFanpage({access_token: user.accessToken}, true);
            if(params.name){
              responeFb = responeFb.filter(fb => {
                return utility.removeUnicode(fb.name).toLowerCase().indexOf(utility.removeUnicode(params.name).toLowerCase()) !== -1;
              });
            }
          }catch(e){
            Logs.error(e);
          }
          if(responeFb.length === 0){
            return;
          }
          return responeFb;
        }));
      arrayFanpages = _.flatten(arrayFanpages);
      arrayFanpages = arrayFanpages.filter(e => e);
      return res.success({ fanpages: arrayFanpages, users: [], groups: []});
    } catch(e) {
      return res.error(e.message, 500);
    }
  },
  getList: async (req, res) => {
    try {
      let params = {
        createdBy: req.user._id
      };
      let users = await userService.find(params);
      if(req.query.name){
        params.name = utility.getValidString(req.query.name);
      }

      if(!users || users.length === 0){
        return res.error('User is not found', 400);
      }
      let arrayFanpages = await Promise.all(
        users.map(async(user) => {
          let responeFb = [];
          let responeGr = [];
          try{
            responeFb =  await facebookHelper.getListFanpage({access_token: user.accessToken});
            if(params.name){
              responeFb = filterByName(responeFb, params.name);
            }
          }catch(e){
            Logs.error(e);
          }
          try {
            responeGr = await facebookHelper.getGroup(user.facebookId, {access_token: user.accessToken});
            if(params.name){
              responeGr = filterByName(responeGr, params.name);
            }
          } catch(e) {
            Logs.error(e);
          }
          if(responeFb.length === 0 && responeGr.length === 0){
            return;
          }
          return {
            fanpages: responeFb,
            groups  : responeGr,
            user
          };
        }));
      arrayFanpages = arrayFanpages.filter(e => e);
      return res.success(arrayFanpages);
    } catch(e) {
      return res.error(e.message, 500);
    }
  }
};
let filterByName = function(array, name) {
  return array.filter(fanpage =>{
    return utility.removeUnicode(fanpage.name).toLowerCase().indexOf(utility.removeUnicode(name).toLowerCase()) !== -1;
  });
};