
const settingController = require('./controller');

module.exports = (route) =>{
  route.post('/settings', settingController.addOrModify);
  return route;
};
