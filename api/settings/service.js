const settingModel = require('./model');

module.exports = {
  find: (query)=>{
    return settingModel.find(query);
  },
  findByAdminSetting: (adminId)=>{
    return settingModel.findOne({createdBy: adminId});
  },
  insertSetting: (setting)=>{
    return settingModel.create(setting);
  },
};
