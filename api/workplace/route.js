
const workplaceController = require('./controller');

module.exports = (route) =>{
  route.get('/workplace/:workplace', workplaceController.getById);
  route.get('/workplace', workplaceController.getList);
  route.post('/workplace', workplaceController.createWorkplace);
  route.post('/workplace/:id/facebook/fanpage', workplaceController.addFanpageIdToWorkplace);
  route.post('/workplace/fanpages', workplaceController.addFanpageToWorkPlace);
  route.delete('/workplace/fanpages', workplaceController.deleteFanpageInWorkplace);
  route.delete('/workplace/channels', workplaceController.deleteChannelInWorkplace);
  route.put('/workplace/:id', workplaceController.updateWorkplace);
  route.delete('/workplace/:id', workplaceController.deleteWorkplace);
  return route;
};