const mongoose = require('mongoose');
const {STATUS, TYPE_WORKPLACE} = require('../../constant');
const Schema = mongoose.Schema;
// Time Rule Example
// 09:00 <=> 900 , 20:30 <=> 2030
const WorkplaceSchema = new Schema({
  name     : { type: String, required: true },
  status   : { type: String, enum: STATUS.ENUM, default: STATUS.ACTIVE },
  type     : { type: String, enum: TYPE_WORKPLACE.ENUM, required: true },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'Account', required: true }
}, {
  timestamps: true, toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
      delete ret.password;
    }
  },
});

module.exports = mongoose.model('Workplace', WorkplaceSchema);
