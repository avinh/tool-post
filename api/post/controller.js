const postService = require('./service');
const fanpageService = require('../fanpage/service');
const settingService = require('../settings/service');
const accountService = require('../account/service');
const userService = require('../user/service');
const commonService = require('../common/service');
// const Logs = require('../../lib/Log.class');
const workplaceService = require('../workplace/service');
const utilities = require('../../helpers/utilities');
const facebookHelper = require('../../helpers/facebook');
const { DATE_FORMAT_SCHEDULE, TYPE_POST, MESSAGE, STATUS }  = require('../../constant');
const moment = require('moment');

module.exports = {
  getDetail: async (req, res)=>{
    try{
      let id = req.params.id;
      let post = await postService.findById(id);
      if(!post) {
        return res.error('Post is not found', 400);
      }
      let user = await userService.getUserActive(req.user._id);
      let multi = [];
      if(post.postTo){
        multi.push(facebookHelper.getFanpageInfo(post.postTo, {access_token: user.accessToken}));
      }
      if(post.place){
        multi.push(facebookHelper.getFanpageInfo(post.place, {access_token: user.accessToken}));
      }
      let [postToFb, placeFb] = await Promise.all(multi);
      let data = post.toObject();
      data.postTo = postToFb;
      data.checkin = placeFb;
      return res.success(data, 'Get Detail Success', 200);
    }catch(e){
      return res.error(e, 500);
    }
  },
  getList: async (req, res) => {
    try {
      let query = req.query;
      let limit = utilities.getValidNumber(query.limit) || 20;
      let page = utilities.getValidNumber(query.page) || 1;
      let skip = utilities.getMongoSkip(page, limit);

      let minLike = utilities.getValidNumber(query.minLike);
      let maxLike = utilities.getValidNumber(query.maxLike);
      let minComment = utilities.getValidNumber(query.minComment);
      let maxComment = utilities.getValidNumber(query.maxComment);
      let minShare = utilities.getValidNumber(query.minShare);
      let maxShare = utilities.getValidNumber(query.maxShare);
      let postAtFrom = utilities.getValidDate(query.postedAtFrom);
      let postAtTo = utilities.getValidDate(query.postedAtTo);

      let isHaveSchedule = utilities.booleanValidate(query.isHaveSchedule);

      let params = {
        totalLike   : setMinMaxQuery(minLike, maxLike),
        totalComment: setMinMaxQuery(minComment, maxComment),
        totalShare  : setMinMaxQuery(minShare, maxShare),
      };
      let workplace = utilities.getValidString(query.workplace);
      params = utilities.removeEmptyFields({
        status        : STATUS.ACTIVE,
        fanpage       : utilities.getValidString(query.fanpage),
        isBookmark    : utilities.booleanValidate(query.isBookmark),
        from          : utilities.getValidString(query.from),
        isPostFacebook: utilities.booleanValidate(query.isPostFacebook),
        type          : utilities.getValidString(query.type),
        postedAt      : setDateQuery(postAtFrom, postAtTo),
        ...params
      });

      //search with schedule post
      if(isHaveSchedule === true){
        params.schedule = { $exists: true };
        if(params.isBookmark === false){
          params.schedule = { ... params.schedule, $gt: new Date() };
        }
      } else if(isHaveSchedule === false){
        params.schedule = { $exists: false };
      }
      // create query with workplace
      if(workplace){
        let queryWorkplace = {
          createdBy: req.user._id,
          _id      : workplace
        };
        let workplaceDB = await workplaceService.findOne(queryWorkplace);
        if(!workplaceDB){
          return res.error('Workplace is not found', 400);
        }
        let fanpagesDb = await fanpageService.find({workplace: workplace});
        let fanpageIds;
        if(fanpagesDb){
          fanpageIds = fanpagesDb.map(fb => fb._id);
        }
        params.fanpage = {$in: fanpageIds};
      } else {
        let queryWorkplace = {
          createdBy: req.user._id
        };

        let workplacesDb = await workplaceService.find(queryWorkplace) || [];
        let workplaceIds = workplacesDb.map(w => w._id);
        let fanpagesDb = await fanpageService.find({workplace: {$in: workplaceIds}});
        let fanpageIds;
        if(fanpagesDb){
          fanpageIds = fanpagesDb.map(fb => fb._id);
        }
        params.fanpage = {$in: fanpageIds};
      }
      let [listPost, total] = await Promise.all([
        postService.find(params, { skip, limit }),
        postService.count(params)
      ]);
      listPost = listPost.map(post => {
        let isBookmark = post.isBookmark;
        if(post.schedule && isBookmark === false && moment().isBefore(post.schedule)){
          isBookmark = true;
        }
        return {
          ...post.toObject(),
          isBookmark    : isBookmark,
          isHaveSchedule: !!post.schedule,
        };
      });
      let users = await userService.find({createdBy: req.user._id}) || [];
      let postToData = await Promise.all(listPost.map(async(post)=>{
        if(!post.postTo){
          return;
        }
        return fanpageService.getFanpageInfoWithUsers(post.postTo, users);
      }));
      for(let index in postToData){
        if(listPost[index] && postToData[index]){
          listPost[index].postTo = {
            fanpageId: postToData[index].fanpageId,
            name     : postToData[index].name,
            picture  : postToData[index].picture,
          };
        }
      }
      let totalPage = utilities.getTotalPage(total, limit);
      return res.success({ page, totalPage, limit, total, listPost }, MESSAGE.GET_SUCCESS, 200);
    } catch (err) {
      res.error(err.message, 500);
    }
  },
  schedulePost: async function(req, res){
    try {
      const content = req.body.content;
      let urls = utilities.getValidArray(req.body.attachments);
      let accessToken = req.body.accessToken;
      let id = req.body._id;
      let from = req.body.from;
      let checkin = req.body.checkin;
      let postTo = req.body.postTo;
      let postToObject = req.body.postToObject;
      let schedule = req.body.schedule;
      let videoUrl = req.body.videoUrl;
      // check schedule
      if (!postTo || !accessToken || !from || !postToObject || ['user', 'group', 'fanpage'].indexOf(postToObject) < 0) {
        return res.error('missing require field');
      }
      let resourceContentDB;
      if(id){
        resourceContentDB = await commonService.getContentResource(id, from);
      }
      if (id && !resourceContentDB) {
        return res.error('postTo or resourceContentDB doesn\'t exists');
      }
      if(videoUrl && (checkin || (urls && urls.length > 0))){
        return res.error('Cant post video with checkin or images');
      }
      let dataPost = {
        isBookmark    : false,
        videoUrl      : videoUrl,
        content       : content,
        attachments   : urls,
        type          : TYPE_POST.SCHEDULE_POST,
        isPostFacebook: false,
      };
      let result;
      if(urls && urls.length > 5){
        urls = urls.slice(0, 5);
      }
      let postToFBID  = postTo;
      if(schedule) {
        // let timezone = new Date().getTimezoneOffset();
        let currentDate = moment().utc().format('YYYY-MM-DD HH:mm');
        // format schedule type unix
        schedule = moment.utc(schedule).format('YYYY-MM-DD HH:mm');
        dataPost.schedule = new Date(schedule);
        if(schedule === 'Invalid date'){
          return res.error('schedule is wrong format, format must be YYYY-MM-DD HH:mm');
        }
        // check schedule >= 15 minutes

        if (moment(schedule).diff(moment(currentDate), 'minute') <= 15) {
          return res.error('please update schedule! Note: time of schedule must be 15 minutes later than the present!');
        }
        schedule = moment(schedule, DATE_FORMAT_SCHEDULE).format('X');
        const timeToCompare = moment().add(20, 'hours').format('X');
        if (schedule <= timeToCompare) {
          result  = await postService.postArticleToFacebook(postToFBID, urls, content, schedule, accessToken, videoUrl, checkin);
          // add attribute for schedule post
        }
        dataPost.isEdited = true;
        dataPost.place = checkin; //for checkin
        dataPost.postToObject = postToObject;
        dataPost.postTo = postTo;
      } else {
        result  = await postService.postArticleToFacebook(postToFBID, urls, content, undefined, accessToken, videoUrl, checkin);
      }
      if (result) {
        dataPost.isPostFacebook = true;
      }
      // update post with db
      if(resourceContentDB){
        utilities.replaceField(resourceContentDB, dataPost);
        resourceContentDB = await resourceContentDB.save();
      } else {
        dataPost.postedAt = new Date();
        dataPost.postId =  result.post_id || result.id;
        resourceContentDB = await postService.create(dataPost);
      }
      let data = resourceContentDB.toObject();
      if(resourceContentDB.place){
        let dataCheckIn = await facebookHelper.getFanpageInfo(resourceContentDB.place, {access_token: accessToken});
        data.checkin = dataCheckIn;
      }
      return res.success(data);
    } catch (err) {
      res.error(err, 500);
    }
  },
  syncPost: async(req, res)=>{
    let params = {
      _id: utilities.getValidString(req.query.workplace)
    };
    params = utilities.removeEmptyFields(params);
    let admin = await accountService.getAccountAdmin();
    let setting = await settingService.findByAdminSetting(admin._id);
    let limit = setting ? setting.limitSync : 20;
    let fanpages = [];
    if(params._id){
      let paramsWorkplace = {...params, createdBy: req.user._id};
      let workplace = await workplaceService.findOne(paramsWorkplace);
      if(!workplace){
        return res.error('workplace is not found', 400);
      }
      fanpages =  await fanpageService.find({workplace: {$in: params._id}});
    } else {
      fanpages = await fanpageService.find(params);
    }
    if(!fanpages || fanpages.length === 0){
      return res.error('not found fanpage of workplace', 400);
    }
    let posts = await postService.syncPost(fanpages, limit);
    return res.success(posts);
  },
  updatePost: async (req, res) => {
    try {
      const id = req.params.id;
      const isBookmark = utilities.booleanValidate(req.body.isBookmark);
      if (!id) {
        return res.error('missing require field Id');
      }
      let post = await postService.findById(id);
      if (!post) {
        return res.error('The post isn\'t exists.');
      }
      //schedule post
      if(post.schedule && moment().isBefore(post.schedule)){
        return res.error('Cant change status bookmark with post have schedule');
      }
      //reset attribute
      post.isBookmark = isBookmark;
      post.schedule = undefined;
      post.postTo = undefined;
      post.place = undefined;
      post.postToObject = undefined;
      post.isEdited = false;

      post = await post.save();
      return res.success(post);
    } catch (err) {
      return res.error(err.message, 500);
    }
  },
  deletePost: async (req, res) => {
    try {
      let id = req.params.id;
      let post = await postService.findById(id);
      if(!post){
        res.error('Post is not found', 400);
      }
      post.status = STATUS.DELETE;
      await post.save();
      return res.success(post, 'success', 200);
    } catch (err) {
      res.error(err.message, 500);
    }
  }
};
let setMinMaxQuery = (min, max) => {
  if(!min && !max){
    return;
  }
  let tmp = {};
  if(min !== undefined && min !== null){
    tmp['$gte'] = min;
  }
  if(max !== undefined && max !== null){
    tmp['$lte'] = max;
  }
  return tmp;
};

let setDateQuery = (from, to) => {
  if(!to && !from){
    return;
  }
  let tmp = {};
  if(to !== undefined && to !== null){
    tmp['$lte'] = utilities.getValidDate(to);
  }
  if(from !== undefined && from !== null){
    tmp['$gte'] = utilities.getValidDate(from);
  }
  return tmp;
};
